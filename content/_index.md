## About Me

I'm a software / site reliability engineer based out of Santa Barbara, California.

This blog will serve as a centralized place for me to publish articles about my thoughts on engineering, as well as specific "war stories" I've accumulated from my time in the industry, along with other unrelated interests of mine.

Please don't hesitate to reach out if you would like feedback on a project you're working on, or if you just want to discuss one of the articles I've published. (Pull requests welcome!)
